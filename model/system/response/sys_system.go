package response

import "ginmode/config"

type SysConfigResponse struct {
	Config config.Server `json:"config"`
}
