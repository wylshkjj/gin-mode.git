package request

import (
	"ginmode/model/common/request"
	"ginmode/model/system"
)

type SysOperationRecordSearch struct {
	system.SysOperationRecord
	request.PageInfo
}
