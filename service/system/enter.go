package system

type ServiceGroup struct {
	JwtService
	InitDBService
	SystemConfigService
	OperationRecordService
}
