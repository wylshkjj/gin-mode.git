package main

import (
	"ginmode/core"
	"ginmode/global"
	"ginmode/initialize"
	"ginmode/utils/translate"
	"go.uber.org/zap"
)

func main() {
	global.GVA_VP = core.Viper()       // 初始化Viper
	initialize.OtherInit()             // JWT 过期配置
	global.GVA_LOG = core.Zap()        // 初始化zap日志库
	zap.ReplaceGlobals(global.GVA_LOG) //日志
	global.GVA_DB = initialize.Gorm()  // gorm连接数据库
	initialize.Timer()                 //定时任务
	initialize.DBList()                // 数据库
	// support multilanguage
	global.GVA_TRANSLATOR = translate.Translator{} // create translator inestance  here
	global.GVA_TRANSLATOR.InitTranslator(global.GVA_CONFIG.Language.Language, global.GVA_CONFIG.Language.Dir)
	// end of adding
	if global.GVA_DB != nil {
		initialize.RegisterTables(global.GVA_DB) // 初始化表
		// 程序结束前关闭数据库链接
		db, _ := global.GVA_DB.DB()
		defer db.Close()
	}
	core.RunWindowsServer()
}
