# gin-mode

#### 介绍
gin框架的项目架构mode

#### 软件架构
```
├── gin-mode
    ├── handler         (api层)
    ├── config          (配置包)
    ├── core            (核心文件)
    ├── docs            (swagger文档目录)
    ├── global          (全局对象)                    
    ├── initialize      (初始化)                        
    │   └── internal    (初始化内部函数)                            
    ├── middleware      (中间件层)                        
    ├── model           (模型层)                    
    │   ├── request     (入参结构体)                        
    │   └── response    (出参结构体)                            
    ├── packfile        (静态文件打包)                        
    ├── resource        (静态资源文件夹)                                             
    │   └── lang        (多语言)                            
    ├── router          (路由层)                    
    ├── service         (service层)                    
    ├── source          (source层)                    
    └── utils           (工具包)                    
        ├── timer       (定时器接口封装)                        
        └── upload      (oss接口封装)  
```



#### 安装教程

> 项目

1.  克隆项目：git clone https://gitee.com/wylshkjj/gin-mode.git
2.  进入gin-mode文件夹：cd gin-mode
3.  使用 go mod 并安装go依赖包：go mod tidy
4.  本地run方式运行：go run main.go -c ./config.yaml

> swagger自动化API文档

1. 下载swagger：go get -u github.com/swaggo/swag/cmd/swag

2. 生成API文档：

   ```shell
   cd gin-mode
   swag init
   ```

3. 执行上面的命令后，server目录下会出现docs文件夹里的 `docs.go`, `swagger.json`, `swagger.yaml` 三个文件更新，启动go服务之后, 在浏览器输入 [http://localhost:8080/swagger/index.html](https://gitee.com/link?target=http%3A%2F%2Flocalhost%3A8080%2Fswagger%2Findex.html) 即可查看swagger文档

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
